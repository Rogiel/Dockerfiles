IMAGES := build/cpp
IMAGES += build/engine
IMAGES += webdav
IMAGES += jupyterlab

all: $(IMAGES)
$(IMAGES):
	$(MAKE) --file=$(CURDIR)/image.mk -C $@ TAG=$@ all

.PHONY: all $(IMAGES)
.DEFAULT:
	$(MAKE) --file=$(CURDIR)/image.mk -C $@ TAG=$@ all
