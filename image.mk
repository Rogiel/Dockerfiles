CI_REGISTRY_IMAGE 	?= registry.docker.com
CI_COMMIT_SHA 		?= HEAD
CI_COMMIT_REF_SLUG 	?= HEAD

IMAGE_NAME = $(TAG)

all: build push

build: Dockerfile
	echo "Building $(TAG)..."

	docker pull ${CI_REGISTRY_IMAGE}/${IMAGE_NAME}:${CI_COMMIT_REF_SLUG} || true

	docker build 																\
		--cache-from ${CI_REGISTRY_IMAGE}/${IMAGE_NAME}:${CI_COMMIT_REF_SLUG} 	\
		-t ${IMAGE_NAME}:${CI_COMMIT_SHA} 										\
		-f $(CURDIR)/Dockerfile 												\
		--build-arg CI_REGISTRY_IMAGE=${CI_REGISTRY_IMAGE} 						\
		--build-arg CI_COMMIT_REF_SLUG=${CI_COMMIT_REF_SLUG} 					\
		--build-arg CI_COMMIT_SHA=${CI_COMMIT_SHA} 								\
		$(CURDIR)

push: Dockerfile
	echo "Pushing $(TAG)..."

	docker tag ${IMAGE_NAME}:${CI_COMMIT_SHA} ${CI_REGISTRY_IMAGE}/${IMAGE_NAME}:latest
	docker tag ${IMAGE_NAME}:${CI_COMMIT_SHA} ${CI_REGISTRY_IMAGE}/${IMAGE_NAME}:${CI_COMMIT_REF_SLUG}
	docker push ${CI_REGISTRY_IMAGE}/${IMAGE_NAME}:${CI_COMMIT_REF_SLUG}
	if [ "${CI_COMMIT_REF_SLUG}" = "master" ]; then docker push ${CI_REGISTRY_IMAGE}/${IMAGE_NAME}:latest; fi
